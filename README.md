このレポジトリについて
---
GitLab.comにおけるDockerイメージのビルドと利用の検証に利用

- GitLabレポジトリ: https://gitlab.com/keiichiroy/gitlab-k8s-deploy
- コンテナレジストリ: registry.gitlab.com:5000/keiichiroy/gitlab-k8s-deploy

### 主なフォルダ構成
- `Pipfile(.lock)`: pipenvでPythonプロジェクトを構成
- `src`: 簡単なPythonスクリプトを複数格納
- `manifests`: Kubernetesリソース定義(下記参照)
- ほかコンテナやCI/CD関連の記載など
    - `Dockerfile`: Python3.7系で動作構成
    - `.gitlab-ci.yml`: コンテナのビルド処理

### 全体の手順[^1]

1. コードの変更をGitLabへプッシュ
2. Dockerイメージのビルドと保存
3. Kubernetes環境へデプロイ

以下、2~3について記載する

#### 手順2: Dockerイメージのビルドと保存
GitLab CI/CDを用いたコンテナビルドは、いわゆる「Docker-in-Docker」な環境のため、コンテナイメージのビルドは少し工夫が必要。具体的には以下2つの方法がある(今回は2を採用)

1. 特権コンテナでの実行を許可する
1. 特権不要のビルドツール([kaniko](https://github.com/GoogleContainerTools/kaniko))を利用

#### 手順3: Kubernetes上へデプロイ
YAMLマニフェストを手動で定義・適用[^4]する。作業は以下の2点。

1. **Secret作成:** GitLabのレジストリ認証用
1. **CronJob作成:** 各スクリプト毎に定義[^3]

##### i) Secret作成
GitLabのプライベートレジストリにアクセスし、Dockerイメージを取得するための認証情報を保存する。


1. GitLabにて認証用のPATを作成(画像参照)
`read_registry`と`write_registry`権限を付与する
![](images/gitlab_create_pat.png)

1. PAT文字列を用いて、[こちらのQiita記事](https://qiita.com/sekikatsu/items/0407a01ca46e16eda3ca#gitlab%E3%83%88%E3%83%BC%E3%82%AF%E3%83%B3%E3%82%92%E4%BD%BF%E3%81%A3%E3%81%A6secret%E3%82%92%E4%BD%9C%E3%82%8B)を参照にSecretを作成する[^5]

##### ii) CronJob作成
src配下のスクリプトを実行するCronJobマニフェストのひな型を作成する。

1. 以下のようなkubectl createコマンドでひな型となるYAMLマニフェストを出力し、ファイルに保存(ここでは`01-hello.yaml`とする)[^2]

    ※5分おきに`src/01_hello.py`を実行するCronJobの例
    ```bash
    kubectl create cronjob 01-hello \
      --image=registry.gitlab.com/keiichiroy/gitlab-k8s-deploy:latest \
      --schedule="*/5 * * * ?" \
      --dry-run=client \
      -o yaml \
      -- pipenv run python "src/01_hello.py"
    ```

1. YAML保存後、環境変数やDockerプライベートレジストリの認証(Secret)を追加後し`kubectl apply`する。
    ```
    kubectl apply -f 01-hello.yaml
    ```
    ※[03-envprint.yaml](./manifests/03-envprint.yaml)の`env`や`imagePullSecrets`を参照のこと。これらの指定がCLIからできないため、具体的にYAMLを起こして修正する必要がある。

### 参考URL
- [Building images with kaniko and GitLab CI/CD | GitLab](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)
- [Pull an Image from a Private Registry | Kubernetes](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)

[^1]: CI/CDの利用範囲はレジストリへのプッシュまで。Kubernetes環境へのデプロイはGitLab側から接続設定が必要。
[^2]: `--dry-run`や`-o yaml`により、実際の適用はせずYAML出力だけ行う。
[^3]: k8s上で一回だけスクリプトを実行したい場合は`kubectl create pod`でポッドを作成する等の方法がある([kubectlのリファレンス](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#create)を参照)
[^4]: CI/CDにて、Gitレポジトリ内に保存したマニフェストを適用する(いわゆるGitOps的な)運用により、自動化を検討できる。
[^5]: かなり手作業を行ったが、手元のCLIで`docker login`した状態から[ほぼワンライナーで作成できる方法](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#registry-secret-existing-credentials)もある(未検証)