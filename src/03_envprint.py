#!/usr/bin/env python

import os

def main():

    print("Script 3: Print .env or Environment Value\n")
    print("DATABASE_URL: {}".format(os.environ.get('DATABASE_URL')))

if __name__ == '__main__':
    main()