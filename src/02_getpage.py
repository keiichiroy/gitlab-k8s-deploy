#!/usr/bin/env python

import requests

def main():

    print("Script 2: just fetch (and print) example.com HTML\n")

    response = requests.get('http://example.com')
    print(response.text)

if __name__ == '__main__':
    main()