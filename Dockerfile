FROM python:3.7
WORKDIR /opt/app
COPY . /opt/app
RUN python -V && pip install pipenv && pipenv install
CMD ["python", "-V"]
